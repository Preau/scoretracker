<?php
//Init
require_once('config.php');
$database = new Database();

//Month options for filters
$months = array(
    1 => 'Januari',
    2 => 'Februari',
    3 => 'Maart',
    4 => 'April',
    5 => 'Mei',
    6 => 'Juni',
    7 => 'Juli',
    8 => 'Augustus',
    9 => 'September',
    10 => 'Oktober',
    11 => 'November',
    12 => 'December'
);

//Year options for filters
$years = array();
for($year = date('Y'); $year >= 2014; $year--) {
    $years[] = $year;
}

//Retrieve available players
$players = $database->getPlayers();

//Retrieve the order of players
if(!file_exists('order.json')) {
    file_put_contents('order.json', json_encode(array()));
}
$order = json_decode(file_get_contents('order.json'));
$orderFileChanged = false;

//Remove inactive players from the order file
foreach($order as $index => $playerId) {
    if(!isset($players[$playerId])) {
        $orderFileChanged = true;
        unset($order[$index]);
    }
}
//Add new players to order
foreach($players as $playerId => $playerName) {
    if(!in_array($playerId, $order)) {
        $orderFileChanged = true;
        $order[] = $playerId;
    }
}

//Update the order file if anything changed
if($orderFileChanged) {
    file_put_contents('order.json', json_encode(array_values($order)));
}
?>
<html>
    <head>
        <title>Tafelvoetbal Scores</title>

        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

        <link href="style.css" type="text/css" rel="stylesheet">

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <script src="http://code.highcharts.com/highcharts.js"></script>

        <script src="script.js"></script>
    </head>
    <body>
        <div id="queue">
            <? foreach($order as $playerId) { ?>
                <div class="queueitem"><?=$players[$playerId]?></div>
            <? } ?>
        </div>

        <div id="filter-form">
            <input id="filter-month" type="radio" name="type" value="month" checked> <label for="filter-month">Maand</label>
            <input id="filter-year" type="radio" name="type" value="year"> <label for="filter-year">Jaar</label>
            <input id="filter-total" type="radio" name="type" value="total"> <label for="filter-total">Totaal</label>
            <input id="filter-last30" type="radio" name="type" value="last30"> <label for="filter-last30">Laatste 30 Dagen</label>
            <select name="month">
                <? foreach($months as $month => $monthName) {
                    $selected = $month == date('n') ? 'selected' : '';
                    echo '<option value="'.$month.'" '.$selected.'>'.$monthName.'</option>';
                } ?>
            </select>
            <select name="year">
                <? foreach($years as $year) {
                    $selected = $year == date('Y') ? 'selected' : '';
                    echo '<option value="'.$year.'" '.$selected.'>'.$year.'</option>';
                } ?>
            </select>
            <button id="refresh-graphs">Vernieuwen</button>
        </div>

        <div id="nogames">
            In deze periode is niet gespeeld
        </div>

        <div class="graph" id="bar-graph">
        </div>
        <div class="graph" id="line-graph">
        </div>

        <table id="result-table">
            <tr>
                <th>Datum</th>
                <? foreach($players as $playerName) { ?>
                    <th colspan="3"><?=$playerName?></th>
                <? } ?>
            </tr>
        </table>
    </body>
</html>